# teamlabオンラインスキルアップチャレンジ Step2

------------------------------

## ToDoリスト（フロントエンド+サーバサイドバージョン）を作ろう

[チームラボオンラインスキルアップチャレンジ](http://team-lab.github.io/skillup-nodejs/)
[Step2 最終課題](http://team-lab.github.io/skillup-nodejs/2/10.html)

### 動作環境

以下のブラウザは動作確認をしました

  + Google Chrome 41.0.2272.118 (64-bit)

### 動作方法

    git clone git@bitbucket.org:yuzu441/teamlabstep2.git
    npm install
    bower install
    gulp release

### 制作環境と使用技術

#### 制作環境

  + WebStorm 10.0.1

#### 使用技術 & フレームワーク

  + Javascript
    - Node.js
    - Express
    - Knockout.js
    - Sammy.js
    - jQuery
    - Mongoose
  + ビルドツール & パッケージ管理
    - npm
    - bower
    - gulp
  + CSS
    - sass
  + DB
    - MongoDB