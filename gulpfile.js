var gulp = require("gulp");
var sass = require("gulp-sass");
var coffee = require("gulp-coffee");
var plumber = require("gulp-plumber");//watcherを止めない

var source = {
  'coffee': "js/**/*coffee",
  'sass': "css/**/*scss",
  'serverModule': 'server_modules/**/*coffee'
};

//リリース用
gulp.task('release', ['sass', 'coffee', 'crouter', 'cservermodule']);

//sassコンパイル
gulp.task('sass', function(){
  gulp.src(source.sass, {base: "css"})
    .pipe(plumber())
    .pipe(sass())
    .pipe(gulp.dest("dest/css"));
});

//coffeeコンパイル
gulp.task('coffee', function(){
  gulp.src(source.coffee, {base: 'js'})
    .pipe(plumber())
    .pipe(coffee())
    .pipe(gulp.dest("dest/js"))
});

//自動変換してくれる
gulp.task('watch', function(){
  function log(e) {
    console.log(e.type, e.path);
  }

  var cw = gulp.watch("js/**/*coffee", ['coffee']);
  var sw = gulp.watch("css/**/*scss", ['sass']);

  cw.on('change', log);
  sw.on('change', log);
});

//express用
gulp.task('serverWatch', function(){
  var sw = gulp.watch("app.coffee", ['crouter']);
  var sm = gulp.watch(source.serverModule, ['cservermodule'])
});

gulp.task('crouter', function(){
  gulp.src("app.coffee")
    .pipe(plumber())
    .pipe(coffee())
    .pipe(gulp.dest(""));
});

gulp.task('cservermodule', function(){
  gulp.src(source.serverModule)
    .pipe(plumber())
    .pipe(coffee())
    .pipe(gulp.dest("server_modules"))
});

var del = require("del");

gulp.task('clean', function(cb) {
  del('dest');
  del('app.js');
  del('server_modules/**/*js', cb)
});