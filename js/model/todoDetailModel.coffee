"use strict"

class @TodoDetailModel

  _HOST_URL = "http://localhost:3000/api/task"

  constructor: () ->
    @tasks = ko.observableArray()
    @listName = ko.observable ""

  getTasks: (listId) ->
    $.ajax {
      type: "GET"
      url: _HOST_URL
      dataType: "json"
      data: {
        listId: listId
      }
      success: (data) =>
        if data?
          @listName data.name
          mapedData = data.tasks.map (e) ->
            return new Task e._id, e.name, e.period, e.createDate, e.status
          mapedData.sort (first, second) -> #createDateでソート
            firstVal = first.createDate()
            secondVal = second.createDate()
            return if firstVal == secondVal then 0 else if firstVal > secondVal then -1 else 1
          @tasks mapedData
    }

  changeStatus: (listId, taskId, status) ->
    $.ajax {
      type: "POST"
      url: _HOST_URL + "/change"
      dataType: "json"
      contentType: "application/json"
      data: JSON.stringify {
        listId: listId
        taskId: taskId
        status: status
      }
      success: (data) =>
        filterd = data.filter (e) ->
          return e._id is taskId
        @tasks().forEach (e) ->
          if e.taskId() is taskId
            e.status filterd[0].status
        return
    }

  createTask: (listId, taskName, period) ->
    $.ajax {
      type: "POST"
      url: _HOST_URL + "/create"
      dataType: "json"
      contentType: "application/json"
      data: JSON.stringify {
        listId: listId
        taskName: taskName
        period: period
      }
      success: (data) =>
        newTask = new Task data._id, data.name, data.period, data.createDate, data.status
        @tasks.splice 0, 0, newTask
    }

###
  取得したタスク用のクラス
###
class Task
  constructor: (taskId, taskName, period, createDate, status) ->
    @taskId = ko.observable taskId
    @taskName = ko.observable taskName
    @period = ko.observable new Date(period)
    @createDate = ko.observable new Date(createDate)
    @status = ko.observable status

    @showPeriod = ko.pureComputed () =>
      return "期間：" + _dateMapper @period()
    @showCreateDate = ko.pureComputed () =>
      return "作成日：" + _dateMapper @createDate()
    @printStatusMsg = ko.pureComputed () =>
      if @status() then "完了" else "未完了"
    @statusDoing = ko.pureComputed () =>
      return !@status()

  _dateMapper = (dateObj) ->
    return dateObj.getFullYear() + "年" + (dateObj.getMonth() + 1) + "月" + dateObj.getDate() + "日"