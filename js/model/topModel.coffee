"use strict"

class @TopModel
  _HOST_URL = "http://localhost:3000/api"
  constructor: ->
    @lists = ko.observableArray()

  getList: ->
    $.ajax {
      type: "GET"
      url: _HOST_URL + '/list'
      success: (data) =>
        mapLists = data.map (e) ->
          newObj = new ListModel e.id, e.name, e.createDate, e.period, e.todoCnt, e.doneCnt
          return newObj
        mapLists.sort (first, second) -> #createDateでソート
          firstVal = first.createDate()
          secondVal = second.createDate()
          return if firstVal == secondVal then 0 else if firstVal > secondVal then -1 else 1
        @lists mapLists
        return
      error: (err) ->
        console.log err
    }
    return

  addList: (listName, cb)->
    $.ajax {
      type: "POST"
      url: _HOST_URL + '/list/create'
      data: JSON.stringify({
        name: listName
      })
      success: (d) =>
        console.log d
        newList = new ListModel d.id, d.name, d.createDate, null, 0, 0
        @lists.splice 0, 0, newList
        cb false #成功時ifで分けてるので
      error: (err) ->
        console.log "err"
        cb err
      dataType: 'json'
      contentType: "application/json"
    }

###
  リスト用のモデル
###
class ListModel
  constructor: (id, name, createDate, period, todoCnt, doneCnt) ->
    @id = ko.observable id
    @name = ko.observable name
    @createDate = ko.observable new Date(createDate)
    @period = ko.observable new Date(period)
    @todoCnt = ko.observable todoCnt
    @doneCnt = ko.observable doneCnt
    @detailUrl = ko.pureComputed () =>
      return "#/todoDetail/" + @id()
    @counterMsg = ko.pureComputed () =>
      if @todoCnt() is 0
        return "Todoはありません"
      else
        return @todoCnt() + "個中" + @doneCnt() + "個がチェック済み"
    @periodLabel = ko.pureComputed () =>
      if @todoCnt() is 0
        return ""
      else
        return "〜 " + @period().getFullYear() + "年" + (@period().getMonth() + 1) + "月" + @period().getDate() + "日"