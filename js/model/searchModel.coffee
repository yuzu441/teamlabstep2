"use strict"

###
  検索用のモデル
###
class @SearchModel
  _HOST_URL = "http://localhost:3000/api/task"

  constructor: () ->
    @findTasks = ko.observableArray()

  search: (searchWord, cb) ->
    $.ajax {
      type: "GET"
      url: _HOST_URL + "/search"
      data: {
        searchWord: searchWord
      }
      dataType: "json"
      success: (data) =>
        if data? and data.length isnt 0
          mapedData = data.map (e) ->
            return new findTask e.listId, e.listName, e.taskName, e.period, e.createDate
          @findTasks mapedData
          cb()
        else
          @findTasks []
          cb()
    }

###
  見つかったタスク用クラス
###

class findTask
  constructor: (listId, listName, taskName, period, createDate) ->
    @listId = ko.observable listId
    @listName = ko.observable listName
    @taskName = ko.observable taskName
    @period = ko.observable new Date(period)
    @createDate = ko.observable new Date(createDate)

    @showListName = ko.pureComputed () =>
      return "リスト: " + @listName()
    @showPeriod = ko.pureComputed () =>
      return "期間： " + _dateMapper @period()
    @showCreateDate = ko.pureComputed () =>
      return "作成日： " + _dateMapper @createDate()

  _dateMapper = (dateObj) ->
    return dateObj.getFullYear() + "/" + (dateObj.getMonth() + 1) + "/" + dateObj.getDate()