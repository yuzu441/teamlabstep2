"use strict"

class @SearchViewModel
  constructor: (searchModel) ->
    @model = searchModel
    @findTasks = searchModel.findTasks

    @searchText = ko.observable ""
    @errorFlag = ko.observable false #true is error
    @msgBox = ko.observable ""

  search: () ->
    searchWord = @searchText()
    if searchWord.length > 0
      @model.search searchWord, =>
        if @findTasks().length is 0
          @errorFlag true
          @msgBox "対象のtodoは見つかりません"
        else
          @errorFlag false
          @msgBox @findTasks().length + "件見つかりました"
    else
      @findTasks []
      @errorFlag true
      @msgBox "検索文字列を入力してください"
    return false