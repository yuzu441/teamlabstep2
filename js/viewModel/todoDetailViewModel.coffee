"use strict"
class @TodoDetailViewModel
  _DateRegex = "[0-2]"
  constructor: (todoDetailModel, listId) ->
    @model = todoDetailModel
    @tasks = todoDetailModel.tasks
    @listId = ko.observable listId
    @listName = todoDetailModel.listName

    @errorFlag = ko.observable false # true is error
    @msgBox = ko.observable ""
    @msgBoxStatus =  ko.pureComputed () ->
      if @tasks.length > 0
        @errorFlag false
        @msgBox ""
      else
        @errorFlag true
        @msgBox "ToDoが作成されていません。"
      return

    @createTaskName = ko.observable ""
    @createTaskPeriod = ko.observable ""

  createTask: () ->
    listId = @listId()
    taskName = @createTaskName()
    period = @createTaskPeriod()
    @model.createTask(listId, taskName, period)
    @createTaskName ""
    @createTaskPeriod ""

  statusBtnClick: (vm, data) ->
    @model.changeStatus(@listId(), data.taskId(), !data.status())
    return



