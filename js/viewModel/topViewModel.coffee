"use strict"

class @TopViewModel
  constructor: (topModel) ->
    @model = topModel
    @lists = topModel.lists

    @msgBox = ko.observable ""
    @createTextForm = ko.observable ""
    @errorFlag = ko.observable false #true is error

  readList: () ->
    @model.getList()

  addList: () ->
    listName = @createTextForm()
    if listName.length > 30
      @msgBox "リストの名称は30文字以内にしてください"
      @errorFlag true
    else if listName.length <= 0
      @msgBox "リスト名を入力してください"
      @errorFlag true
    else
      @model.addList listName, (err) =>
        if err
          @msgBox "作成に失敗しました"
          @errorFlag true
        else
          @createTextForm ""
          @errorFlag false
          @msgBox "新しいtodoリストが作成されました"