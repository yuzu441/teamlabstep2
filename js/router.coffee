@app = $.sammy "#main", () ->

  @_checkFormSubmission = (form) ->
    return (false)

  #toppage
  @get "#/top", ->
    @partial "top.html"
    return

  #todo詳細
  @get "#/todoDetail/:id", ->
    console.log @params['id']
    @partial "todoDetail.html"
    return


  #検索用ルーティング
  @get "#/search", ->
    @partial "search.html"
    return

$ =>
  @app.run("#/top")