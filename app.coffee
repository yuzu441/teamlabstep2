express = require "express"
app = express()
todoApi = require "./server_modules/taskapi.js"
bodyParser = require "body-parser"

app.use bodyParser.json()#requestbody読み取るようらしい

app.use express.static("bower_components")
app.use express.static("views")
app.use express.static("dest")

apiUri = '/api/'
error = "error"

###
  リスト関連API
###

#list取得
app.get apiUri + 'list', (req, res)->
  todoApi.listApi.get (data) ->
    res.send data

#list作成
app.post apiUri + 'list/create', (req, res) ->
  msgBody = req.body
  data = todoApi.listApi.add msgBody.name
  res.send data

###
  タスク関連API
###

#task一覧取得
app.get apiUri + 'task', (req, res) ->
  listId = req.query.listId
  todoApi.taskApi.get listId, (data) ->
    res.send data

#task作成
app.post apiUri + 'task/create', (req, res) ->
  msgBody = req.body
  todoApi.taskApi.add msgBody.listId, msgBody.taskName, msgBody.period, (data) ->
    res.send data

#ステータス変更
app.post apiUri + 'task/change', (req, res) ->
  msgBody = req.body
  todoApi.taskApi.change msgBody.listId, msgBody.taskId, msgBody.status, (data) ->
    if data == error
      res.status 400
      res.send error
    else
      res.send data

#検索
app.get apiUri + 'task/search', (req, res) ->
  searchWord = req.query.searchWord
  todoApi.taskApi.search searchWord, (data) ->
    res.send data

console.log "startup server 3000 port"
app.listen 3000