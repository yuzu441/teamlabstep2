'use strict'

taskApi = do ->
  mongoose = require('mongoose')
  mongoose.connect('mongodb://localhost/teamlab2')

  Schema = mongoose.Schema

  # Mongoのスキーマー
  # String, Number, Date
  # Buffer, Boolean, Mixed
  # ObjectId, Array

  TodoSchema = new Schema ({
    name: {type: String, required: true} #タスク名
    period: {type: Date, required: true} #締め切り
    createDate: {type: Date} #作成日
    status: {type: Boolean, default: false} #タスクステータス todo->false, done->true
  })

  mongoose.model 'todo', TodoSchema

  listSchema = new Schema {
    #オブジェクトのidで紐付
    name: {type: String, required: true} #リストタイトル
    createDate: {type: Date} #作成日
    tasks: ['todo'] #リストに登録されているタスク一覧
  }
  mongoose.model 'list', listSchema

  ###
      リスト関連のAPI
  ###
  #リスト一覧取得
  getList = (cb)->
    ListModel = mongoose.model 'list'
    ListModel.find {}, (err, lists) ->
      if !err
        cb lists.map (e) ->
          retObj = {} #ほしい情報だけのオブジェクト
          retObj.id = e._id
          retObj.name = e.name
          retObj.createDate = e.createDate
          retObj.todoCnt = e.tasks.length
          retObj.doneCnt = e.tasks.filter (task) -> #doneの数を数える
            return task.status == true
          .length
          retObj.period = Math.min.apply null, e.tasks.map (e) ->
            dateTime = new Date(e.period).getTime()
            return dateTime
          return retObj

  #リスト追加
  addList = (listName) ->
    ListModel = mongoose.model 'list'
    newList = new ListModel()
    newList.name = listName
    newList.createDate = Date.now()
    newList.tasks = []
    newList.save (err) ->
      if err
        return err
    return newList

  ###
      タスク関連API
  ###
  #タスク追加
  addTask = (listId, taskName, period, cb) ->
    ListModel = mongoose.model 'list'
    #保存するリストを探す
    ListModel.findOne {_id: listId}, (err, post) ->
      if !err
        TodoModel = mongoose.model 'todo'
        newTask = new TodoModel()
        newTask.name = taskName
        newTask.period = period
        newTask.createDate = Date.now()
        post.tasks.push newTask
        post.save (err, listData) ->
          if err
            console.log err
          tasks = listData.tasks.filter (task) -> #作成したものを一覧から探す
            return task.name == newTask.name and task.createDate == newTask.createDate
          cb tasks[0] #filterで絞り込んでいるので複数変えることはない
          return
      return


  #タスクのステータス変更
  changeStatus = (listId ,taskId, status, cb) ->
    ListModel = mongoose.model 'list'
    ListModel.findOne {_id: listId}, (err, findList) -> #タスクが保存されているリストを探す
      if !err
        tasks = findList.tasks.map (e) -> #変更されたタスクのリストを返す
          if taskId isnt String(e._id)
            return e #変更したいタスクではないので直接かえす
          else
            changeObj = e
            changeObj.status = status
            return changeObj #ステータスだけ変える
        findList.tasks = tasks
        findList.markModified "tasks" #tasksを変更したことを明示する。これがないと動かなかった
        findList.save (err) ->
          if err
            cb "error"
          else
            cb tasks
      else
        cb "error"
      return
    ###
      db.lists.update({"tasks._id": ObjectId("55216f939a596b64177cd238")},{$set: {"tasks.$.status": false}})
    ###

  #リストのタスク取得
  getTask = (listId, cb) ->
    ListModel = mongoose.model 'list'
    ListModel.findOne {_id: listId}, (err, list) ->
      if !err
        retObj = {}
        retObj.name = list.name
        retObj.id = list._id
        retObj.tasks = list.tasks
        cb retObj
      return

  #前方一致検索
  searchTask = (searchWord, cb) ->
    ListModel = mongoose.model 'list'
    re = new RegExp "^" + searchWord
    ListModel.find {"tasks.name": {"$regex": re}}, (err, lists) ->
      if lists is undefined or lists is null
        cb []

      retAry = [] #返却用の配列
      lists.forEach (list) ->
        list.tasks.forEach (task) ->
          if re.test task.name
            findObj = {}
            findObj.listId = list._id
            findObj.listName = list.name
            findObj.taskName = task.name
            findObj.period = task.period
            findObj.createDate = task.createDate
            retAry.push findObj

      cb retAry
      return

    ###
      db.lists.find({"tasks.name": {$regex: '^t'}})
    ###


  #APIオブジェクトを返す
  return {
    listApi: {
      get: getList
      add: addList
    }
    taskApi: {
      add: addTask
      change: changeStatus
      get: getTask
      search: searchTask
    }
  }

module.exports = taskApi